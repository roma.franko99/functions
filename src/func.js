const { listOfPosts2 } = require("./posts");

const getSum = (str1, str2) => {
  if(typeof str1 === 'string' && typeof str2 === 'string'){
    if((/\d/.test(str1) || str1.length===0)  && (/\d/.test(str2) || str2.length===0)){

      const largest = str1.length > str2.length ? str1.length : str2.length;
      let s1 = str1.padStart(largest, '0').split('').reverse().map(x => +x);
      let s2 = str2.padStart(largest, '0').split('').reverse().map(x => +x);
      let spec = 0;
      const result = [];
      for (let i = 0; i < s1.length; i++) {
        let sum = s1[i]+s2[i]+spec;
        if(sum>=10){
          sum = sum -10;
          result.push(sum)
          spec = 1;
        } else {
          result.push(sum)
          spec = 0;
        }
      }
      return result.reverse().join('')
    };
  }
  return false;
};

// console.log(getSum('','165156162'))

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0
  let comments = 0
  listOfPosts.forEach(el => {
    if(el.author === authorName){
      posts++
    }
    el.comments?.forEach(comment => {
      if(comment.author === authorName){
        comments++
      }
    })
  });

  return `Post:${posts},comments:${comments}`;
};

// console.log(getQuantityPostsByAuthor(listOfPosts2, 'Rimus'))

const tickets=(people)=> {
  const money ={m25: 0, m50: 0, m100: 0,}
  for (let i = 0; i < people.length; i++) {
    const x = people[i];
    if(money.m25<0 || money.m50<0){ return "NO" }
   
    if(x===50){
      if(money.m25 > 0){money.m25-- } else {return "NO";}
    }

    if(x === 100){
      if(money.m50 > 0 && money.m25 > 0){money.m50--; money.m25--} else if (money.m25 >= 3) {money.m25 = money.m25 - 3} else {return "NO";}
    }

    x === 25 ? money.m25++ : ''
    x === 50 ? money.m50++ : ''
    x === 100 ? money.m100++ : ''

  }

  return "YES";
};

// console.log(tickets([25, 100]))


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
